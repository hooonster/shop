const Product = require("../models/product");
const mongoDb = require("mongodb");

const objectID = mongoDb.ObjectID;

exports.addProductPage = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false
  });
};
exports.postProduct = (req, res, next) => {
  const title = req.body.title,
    imgURL = req.body.imageUrl,
    price = req.body.price,
    description = req.body.description;

  const product = new Product({
    title: title,
    imageURL: imgURL,
    price: price,
    description: description,
    userID: req.user
  });

  product
    .save()
    .then(result => {
      console.log("CREATED PRODUCT");
      res.redirect("/admin/products");
    })
    .catch(err => {
      console.log(err);
    });
};
exports.editProductPage = (req, res, next) => {
  const editmode = req.query.edit, // query: from url post "?"
    prodID = req.params.productId; //params: from "routes" post ":"
  if (!editmode) {
    return res.redirect("/");
  }
  Product.findById(prodID)
    .then(product => {
      if (!product) {
        return res.redirect("/");
      }
      res.render("admin/edit-product", {
        pageTitle: "Add Product",
        path: "/admin/add-product",
        editing: editmode,
        product: product
      });
    })
    .catch(err => {
      console.log(err);
    });
};
exports.postEditProduct = (req, res, next) => {
  const currEditProductID = req.body.productID;
  const currEditProductTitle = req.body.title,
    currEditProductImg = req.body.imageUrl,
    currEditProductPrice = req.body.price,
    currEditProductDesc = req.body.description;

  Product.findById(currEditProductID)
    .then(product => {
      product.title = currEditProductTitle;
      product.imageURL = currEditProductImg;
      product.description = currEditProductDesc;
      product.price = currEditProductPrice;

      product.save();
    })
    .then(result => {
      console.log(`PRODUCT UPDATED`);
      res.redirect("/admin/products");
    })
    .catch(err => console.log(err));
};

exports.getProducts = (req, res, next) => {
  Product.find()
    .then(products => {
      res.render("admin/products", {
        prods: products,
        pageTitle: "Products (Admin)",
        path: "/admin/products"
      });
    })
    .catch(err => console.log(err));
};

exports.postDeleteProduct = (req, res, next) => {
  const prodID = req.body.prodID;
  Product.findByIdAndDelete(prodID)
    .then(() => {
      res.redirect("/admin/products");
    })
    .catch(err => {
      console.log(err);
    });
};
