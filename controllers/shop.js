const Product = require("../models/product");
const Order = require("../models/order");

exports.getIndex = (req, res, next) => {
  Product.find()
    .then(results => {
      console.log("results", results);
      res.render("shop/index", {
        prods: results,
        pageTitle: "Home",
        path: "/"
      });
    })
    .catch(err => console.log(err));
};

exports.getProducts = (req, res, next) => {
  Product.find()
    .then(results => {
      res.render("shop/product-list", {
        prods: results,
        pageTitle: "Product List",
        path: "/product-list"
      });
    })
    .catch(err => console.log(err));
};

exports.getSpecificProduct = (req, res, next) => {
  const currProdID = req.params.prodID;
  Product.findById(currProdID)
    .then(product => {
      res.render("shop/product-detail", {
        prod: product,
        pageTitle: "Product Detail",
        path: "/products"
      });
    })
    .catch(err => console.log(err));
};

exports.getCart = (req, res, next) => {
  req.user
    .populate("cart.items.productID")
    .execPopulate()
    .then(user => {
      console.log(user.cart.items);
      res.render("shop/cart", {
        pageTitle: "Cart",
        path: "/cart",
        cartItemList: user.cart.items
      });
    })
    .catch(err => {
      console.log(err);
    });
};

exports.postCart = (req, res, next) => {
  const currProdID = req.body.prodID;
  Product.findById(currProdID)
    .then(product => {
      return req.user.addToCart(product);
    })
    .then(result => {
      console.log(result);
      res.redirect("/cart");
    });
};

exports.postDeleteCartItem = (req, res, next) => {
  const productID = req.body.productID;
  req.user
    .deleteFromCart(productID)
    .then(result => {
      res.redirect("/cart");
    })
    .catch(err => console.log(err));
};

exports.getOrders = (req, res, next) => {
  Order.find({ "user.userID": req.user._id })
    .then(orders => {
      return orders;
    })
    .then(orders => {
      res.render("shop/orders", {
        pageTitle: "Orders",
        path: "/orders",
        orders: orders
      });
    })
    .catch(err => console.log(err));
};

exports.postOrder = (req, res, next) => {
  req.user
    .populate("cart.items.productID")
    .execPopulate()
    .then(user => {
      const products = user.cart.items.map(p => {
        return { quantity: p.quantity, product: { ...p.productID._doc } };
      });
      const order = new Order({
        user: {
          name: req.user.name,
          userID: req.user
        },
        products: products
      });
      return order.save();
    })
    .then(result => {
      return req.user.clearCart();
    })
    .then(result => {
      res.redirect("/orders");
    })
    .catch(err => {
      console.log(err);
    });
};
