const express = require("express");

const shopController = require("../controllers/shop");

const router = express.Router();

router.get("/", shopController.getIndex);

router.get("/product-list", shopController.getProducts);

router.get("/cart", shopController.getCart);

router.post("/cart", shopController.postCart);

router.post("/cart-delete-item", shopController.postDeleteCartItem);

router.post("/create-order", shopController.postOrder);

router.get("/products/:prodID", shopController.getSpecificProduct);

router.get("/orders", shopController.getOrders);

module.exports = router;
