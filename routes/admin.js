const express = require("express");
const path = require("path");

const adminController = require("../controllers/admin");

const router = express.Router();

// /admin/add-product => GET
router.get("/add-product", adminController.addProductPage);

// /admin/products => GET
router.get("/products", adminController.getProducts);

router.get("/edit-product/:productId", adminController.editProductPage);

// // /admin/add-product => POST
router.post("/add-product", adminController.postProduct);

router.post("/edit-product", adminController.postEditProduct);

router.post("/delete-product", adminController.postDeleteProduct);

module.exports = router;
