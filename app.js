const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");

const mongoose = require("mongoose");

const adminRoute = require("./routes/admin");
const shopRouter = require("./routes/shop");

const countroller404 = require("./controllers/404");

const User = require("./models/user");

const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use((req, res, next) => {
  User.findById("604e3c1973dc940f1603af80")
    .then(user => {
      req.user = user;
      next();
    })
    .catch(err => {
      console.log(err);
    });
});

app.use("/admin", adminRoute);
app.use("/", shopRouter);

app.use(countroller404.show404Page);

mongoose
  .connect(
    "mongodb+srv://first-user:1234qwer@shop.viica.mongodb.net/shop?authSource=admin&replicaSet=atlas-iivcfq-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true"
  )
  .then(result => {
    User.findOne().then(user => {
      if (!user) {
        const user = new User({
          name: "John",
          email: "john@someEmail.com",
          cart: { items: [] }
        });
        user.save();
      }
    });
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });
