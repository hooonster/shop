const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  cart: {
    items: [
      {
        productID: {
          type: Schema.Types.ObjectId,
          ref: "Product",
          required: true
        },
        quantity: { type: Number, required: true }
      }
    ]
  }
});

userSchema.methods.addToCart = function(product) {
  const cartProductIndex = this.cart.items.findIndex(p => {
    return p.productID.toString() == product._id.toString();
  });
  const cartItemArray = [...this.cart.items];
  let updatedQuantity = 1;

  if (cartProductIndex >= 0) {
    updatedQuantity = this.cart.items[cartProductIndex].quantity + 1;
    cartItemArray[cartProductIndex].quantity = updatedQuantity;
  } else {
    cartItemArray.push({
      productID: product._id,
      quantity: updatedQuantity
    });
  }
  const updatedCart = {
    items: cartItemArray
  };

  this.cart = updatedCart;
  return this.save();
};

userSchema.methods.deleteFromCart = function(prodID) {
  const updatedCartItems = this.cart.items.filter(item => {
    return item.productID != prodID;
  });
  this.cart.items = updatedCartItems;
  return this.save();
};

userSchema.methods.clearCart = function() {
  this.cart = { items: [] };
  return this.save();
};
module.exports = mongoose.model("User", userSchema);

// const mongoDb = require("mongodb");
// const getDb = require("../util/database").getDb;

// const ObjectId = mongoDb.ObjectID;

// class User {
//   constructor(userName, email, cart, id) {
//     this.userName = userName;
//     this.email = email;
//     this.cart = cart; //{items: []}
//     this._id = id;
//   }
//   save() {
//     const db = getDb();
//     return db
//       .collection("users")
//       .insertOne(this)
//       .then()
//       .catch();
//   }
//   addToCart(product) {
//     const cartProductIndex = this.cart.items.findIndex(p => {
//       return p.productID.toString() == product._id.toString();
//     });
//     const cartItemArray = [...this.cart.items];
//     let updatedQuantity = 1;

//     if (cartProductIndex >= 0) {
//       updatedQuantity = this.cart.items[cartProductIndex].quantity + 1;
//       cartItemArray[cartProductIndex].quantity = updatedQuantity;
//     } else {
//       cartItemArray.push({
//         productID: new ObjectId(product._id),
//         quantity: updatedQuantity
//       });
//     }
//     const updatedCart = {
//       items: cartItemArray
//     };

//     const db = getDb();
//     return db
//       .collection("users")
//       .updateOne(
//         { _id: new ObjectId(this._id) },
//         { $set: { cart: updatedCart } }
//       );
//   }

//   getCart() {
//     const db = getDb();
//     const productIDsArray = this.cart.items.map(i => {
//       return i.productID;
//     });
//     return db
//       .collection("products")
//       .find({ _id: { $in: productIDsArray } })
//       .toArray()
//       .then(products => {
//         return products.map(p => {
//           return {
//             ...p,
//             quantity: this.cart.items.find(i => {
//               console.log(i.productID, p._id);
//               return i.productID.toString() == p._id.toString();
//             }).quantity
//           };
//         });
//       });
//   }

//   deleteCartItem(prodID) {
//     const updatedCartItems = this.cart.items.filter(item => {
//       return item.productID != prodID;
//     });
//     const db = getDb();
//     return db
//       .collection("users")
//       .updateOne(
//         { _id: new ObjectId(this._id) },
//         { $set: { cart: { items: updatedCartItems } } }
//       );
//   }

//   submitOrder() {
//     const db = getDb();
//     return this.getCart()
//       .then(products => {
//         const orderItem = {
//           items: products,
//           user: {
//             _id: new ObjectId(this._id),
//             userName: this.userName
//           }
//         };
//         db.collection("orders").insertOne(orderItem);
//       })
//       .then(result => {
//         this.cart = { items: [] };
//         return db
//           .collection("users")
//           .updateOne(
//             { _id: new ObjectId(this._id) },
//             { $set: { cart: { items: [] } } }
//           );
//       });
//   }

//   getOrders() {
//     const db = getDb();
//     return db
//       .collection("orders")
//       .find({ "user._id": new ObjectId(this._id) })
//       .toArray();
//   }
//   static findById(userId) {
//     const db = getDb();
//     return db
//       .collection("users")
//       .findOne({ _id: new mongoDb.ObjectId(userId) })
//       .then(user => {
//         console.log("new user", user);
//         return user;
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   }
// }

// module.exports = User;

// // Sequelize
// // ==================
// // const Sequelize = require('sequelize');

// // const sequelize = require('../util/database');

// // const User = sequelize.define('user', {
// // 	id: {
// // 		type: Sequelize.INTEGER,
// // 		autoIncrement: true,
// // 		allowNull: false,
// // 		primaryKey: true
// // 	},
// // 	name:  Sequelize.STRING,
// // 	email: Sequelize.STRING,
// // })
