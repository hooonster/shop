const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  imageURL: {
    type: String,
    required: true
  },
  userID: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

module.exports = mongoose.model("Product", productSchema);

// const mongoDb = require("mongodb");
// const getDb = require("../util/database").getDb;

// class Product {
//   constructor(title, price, desc, imageUrl, id, userId) {
//     this.title = title;
//     this.price = price;
//     this.desc = desc;
//     this.imageUrl = imageUrl;
//     this._id = id ? mongoDb.ObjectID(id) : null;
//     this.userId = userId;
//   }

//   save() {
//     const db = getDb();
//     let dbOp;
//     if (this._id) {
//       dbOp = db
//         .collection("products")
//         .updateOne({ _id: this._id }, { $set: this });
//     } else {
//       dbOp = db.collection("products").insertOne(this);
//     }
//     return dbOp
//       .then(result => {
//         // console.log("Product(result)", result);
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   }

//   static fetchAll() {
//     const db = getDb();
//     return db
//       .collection("products")
//       .find()
//       .toArray()
//       .then(products => {
//         console.log("products", products);
//         return products;
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   }

//   static findById(prodID) {
//     const db = getDb();
//     return db
//       .collection("products")
//       .find({ _id: new mongoDb.ObjectID(prodID) })
//       .next()
//       .then(product => {
//         console.log("FindByID product", product);
//         return product;
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   }

//   static deleteById(prodId) {
//     const db = getDb();
//     return db
//       .collection("products")
//       .deleteOne({ _id: new mongoDb.ObjectID(prodId) })
//       .then(result => {
//         console.log("Product Deleted");
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   }
// }
// module.exports = Product;
// // Sequelize
// // ==========================================
// // const Sequelize = require('sequelize');

// // const sequelize = require('../util/database');

// // const Product = sequelize.define('product', {
// // 	id: {
// // 		type: Sequelize.INTEGER,
// // 		autoIncrement: true,
// // 		allowNull: false,
// // 		primaryKey: true
// // 	},
// // 	title: {
// // 		type: Sequelize.STRING,
// // 		allowNull: false
// // 	},
// // 	price: {
// // 		type: Sequelize.DOUBLE,
// // 		allowNull: false
// // 	},
// // 	imageUrl: {
// // 		type: Sequelize.STRING,
// // 		allowNull: false
// // 	},
// // 	description: {
// // 		type: Sequelize.STRING,
// // 		allowNull: false
// // 	}
// // })
// // module.exports = Product;

// // Pre-Sequlize
// // ==========================================
// // const fs = require('fs');
// // const path  = require('path');
// // const db = require('../util/database');

// // const Cart = require('./cart')
// // const productDataPath = path.join(
// //     path.dirname(process.mainModule.filename),
// //     'data',
// //     'products.json'
// // )

// // const getProductsFromFile = cb => {
// //     fs.readFile(productDataPath, (err, fileContent) => {
// //       if (err) {
// //         cb([]);
// //       } else {
// //         cb(JSON.parse(fileContent));
// //       }
// //     });
// //   };
// // module.exports = class Product {
// //     constructor(id, title, imgURL, price, desc){
// //         this.id = id;
// //         this.title = title;
// //         this.imgURL = imgURL;
// //         this.price = price;
// //         this.desc = desc;
// //     }

// //     save(){
// //       return db.execute('INSERT INTO products (title, price, description, imageURL) VALUES (?, ?, ?, ?)',
// //         [this.title, this.price, this.desc, this.imgURL])
// //     }

// //     static deleteByID(id){

// //     }

// //     static fetchAll(){
// //         return db.execute('SELECT * FROM products')
// //     }

// //     static findByID(id) {
// //         return db.execute('SELECT * FROM products WHERE id=?', [id]);
// //     }
// // }
